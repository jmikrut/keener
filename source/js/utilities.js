///////////////////////////////////////////////
// REUSABLE UTILITY FUNCTIONS
///////////////////////////////////////////////

//////////////////////////////////////////////////
// TABS - LINKS NEED CLASS OF "TAB" AND HREF SET TO ID OF PROPER PANEL
//////////////////////////////////////////////////

var keenUtilities = {

	///////////////////////////////////////////////
	// TABS
	///////////////////////////////////////////////

	tabs : function() {
		// IF PAGE HAS TABS & IF URL CONTAINS A HASH, SHOW THE HASH TAB & ADD ACTIVE CLASS TO APPROPRIATE BTN
		if ( $('nav.tabs').length > 0 && window.location.hash) {
			var hash = window.location.hash;
			$(hash).siblings().addClass('inactive');
			$(hash).addClass('active');
			$(hash).show();
			var activeLink = $('a[href=\'' + hash + '\']');
			$(activeLink).addClass('active');

			// Add class for gradient position
			var totalLinks = $('nav.tabs a').length;
			var current = $(activeLink).index() + 1;

			var pos = Math.round(current / totalLinks * 100);

			$('.tab-content').attr('data-position', pos);

		// IF PAGE HAS TABS BUT DOES NOT HAVE A HASH, SHOW THE FIRST TAB AND ADD ACTIVE CLASS TO FIRST TAB BTN
		} else if ($('nav.tabs').length > 0 && !window.location.hash) {

			// Add class for gradient position
			$('.tab-content').attr('data-position', 0);

			$('.tab-content li:first-child').addClass('active');
			$('.tab-content li:first-child').show();
			$('nav.tabs a:first-child').addClass('active');
			$('.tab-content li:first-child').siblings().addClass('inactive');
			$('nav.tabs a:first-child').siblings().addClass('inactive');
		};

		$('nav.tabs a').click(function (e) {
			e.preventDefault();

			// Add class for gradient position
			var totalLinks = $('nav.tabs a').length;
			var current = $(this).index() + 1;

			var pos = Math.round(current / totalLinks * 100);

			$('.tab-content').attr('data-position', pos);

			$(this).siblings().removeClass('active');
			$(this).addClass('active');

			var tabPanel = $(this).attr('href');

			$(tabPanel).siblings().fadeOut(400);
			setTimeout(function(){
				$(tabPanel).fadeIn();
			}, 400);

			// ADD HASH TO BROWSER - NOTE: THIS METHOD PREVENTS PAGE JUMPING
			if (history.replaceState) {
				history.replaceState(null, null, tabPanel);
			}
			else {
				location.hash = tabPanel;
			}
		});
	},

	///////////////////////////////////////////////
	// ACCORDION
	///////////////////////////////////////////////

	accordion: function() {

		$('.accordion .head').click(function(){

			$(this).toggleClass('active');

			//Expand or collapse this panel
			$(this).next().slideToggle().toggleClass('active');

			//Hide the other panels
			$(".accordion .content").not($(this).next()).slideUp().removeClass('active');
			$('.accordion .head').not($(this)).removeClass('active');

		});
	},

	minHeight: function(el) {
		var heights = [];

		$(el).each(function() {
			var height = $(this).height();
			heights.push(height);
		});

		var aMax = Math.max.apply.bind(Math.max, Math);
		var tallest = aMax(heights);

		$(el).css('min-height', tallest + 'px');
	},

	//////////////////////////////////////////////////
	// FORM ERROR HANDLING
	//////////////////////////////////////////////////

	// Error handling object takes three arguments -
	  // Where to write the error messages,
	  // What submit button to use,
	  // And an array that contains each field needing validation

	FormValidator: function(errorMsgBox, submitBtn, fields) {

	    // On submit, check each field passed, if no errors, submit - otherwise prevent default and show errors
	    $(submitBtn).click(function (e) {

	        // Errors set to 0
	        var errors = 0,

	        // Error message array
	        errorMsg = [],

	        // Password fields scoped to submit function, we'll use later
	        password = '',
	        confirmPassword = '',

	        // At least six characters, one upper, one lower, one number
	        regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/,

	        // Possible error messages
	        messages = {
	            'generic': 'There was an error with the info entered below.  Please correct the highlighted fields.',
	            'multiple': 'Oops - looks like some stuff below is not quite right.  Take a look.',
	            'password': 'Your password needs at least 6 characters, at least one capital letter, and at least one number.  Special characters ( / ! ? - _ = + ) not allowed.',
	            'confirmPw': 'The passwords entered don\'t match.  Please re-type your password below.',
	            'validEmail': 'Please enter a valid email address.',
	            'fullPhone': 'Please enter a full phone number, including area code.',
	            'radio': 'Please choose at least one option in the list highlighted in red below.',
	        };

	        // Loop thru each field passed
	        $(fields).each(function () {

                // What are the rules for this field?
                switch (this.rules) {
                    case 'text':
                        if ( $(this.field).val() == '' ) {
                            errors++;
                            $(this.field).addClass('error');
                        } else {
                            $(this.field).removeClass('error');
                        };
                        break;

                    case 'password':
                        // Check to see if the password satisfies our requirements
                        if ($(this.field).val() == '' || !regex.test($(this.field).val())) {
                            errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.password);
                            password = $(this.field).val();
                        } else {
                            // Set the password to compare against confirmation pw
                            password = $(this.field).val();
                            $(this.field).removeClass('error');
                        };
                        break;

                    case 'confirmPassword':
                        // Set confirm pw

                        confirmPassword = $(this.field).val();

                        // Compare passwords
                        if ($(this.field).val() == '' || password != confirmPassword) {
                            errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.confirmPw);
                        } else {
                            $(this.field).removeClass('error');
                        };
                        break;

                    case 'email':
                        // Is this a real address?
						var testEmail = /\S+@\S+\.\S+/;
						var results = testEmail.test( $(this.field).val() );

                        if (!results) {
                            errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.validEmail);
                        } else {
                            $(this.field).removeClass('error');
                        };
                        break;

                    case 'phone':
                        // Phone number needs to be at least 10 digits (including area code)
                        if ($(this.field).val().length < 9) {
                            errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.fullPhone);
                        } else {
                            $(this.field).removeClass('error');
                        }
                        break;

                    case 'select':
                        if ($(this.field)[0].selectedIndex <= 0) {
                            errors++;
                            $(this.field).addClass('error');
                        } else {
                            $(this.field).removeClass('error');
                        }
                        break;

                    case 'radio':
                        var radios = new Array($(this.field).find($('input[type=radio]')));
                        var selected = 0;

                        $(radios).each(function () {
                            if ($(this).is(':checked')) {
                                selected = selected + 1;
                            }
                        });
                        if (selected == 0) {
                        	errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.radio);
                        } else {
                            $(this.field).removeClass('error');
                        }
                        break;

                    case 'checkbox':
                        var checkboxes = new Array($(this.field).find($('input[type=checkbox]')));
                        var selected = 0;

                        $(checkboxes).each(function () {
                            if ($(this).is(':checked')) {
                                selected = selected + 1;
                            }
                        });
                        if (selected == 0) {
                        	errors++;
                            $(this.field).addClass('error');
                            errorMsg.push(messages.radio);
                        } else {
                            $(this.field).removeClass('error');
                        }
                        break;
                }
	        });

	        if (errors == 0) {

	            // If there are no errors, just pass this step
	            
	        // If there's an error, prevent the submit and add the error
	        } else if (errors == 1) {

	            errors = 0;
	            e.preventDefault();
	            $(errorMsgBox).find('h4').remove();
	            $(errorMsgBox).find('ul').remove();
	            $('html, body').animate({ scrollTop: '0px' }, 500);

	            if (errorMsg.length <= 0) {
	                errorMsg.push(messages.generic);
	            }

	            $(errorMsgBox).prepend('<h4>' + errorMsg + '</h4>');
	            setTimeout(function () {
	                $(errorMsgBox).slideDown();
	            }, 500);

	        // If there's more than one error, add each error and prevent submit
	        } else {

	            errors = 0;
	            e.preventDefault();
	            $(errorMsgBox).find('h4').remove();
	            $(errorMsgBox).find('ul').remove();
	            $('html, body').animate({ scrollTop: '0px' }, 500);
	            $(errorMsgBox).prepend('<ul></ul>');
	            $(errorMsgBox).prepend('<h4>' + messages.multiple + '</h4>');
	            $(errorMsg).each(function () {
	                $('.error-box ul').append('<li>' + this + '</li>');
	            });
	            setTimeout(function () {
	                $(errorMsgBox).slideDown();
	            }, 500);
	        }
	    });
	}
}