///////////////////////////////////////////////
// INIT
///////////////////////////////////////////////

$(document).ready(function() {

	///////////////////////////////
	// TEST FOR MOBILE
	///////////////////////////////

	var isMobile = false;
	var os = false;

	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	var android =  /Android/i.test(navigator.userAgent);

	if (iOS || android) {
		isMobile = true;
	};

	if (iOS) { os = 'ios'; };
	if (android) { os = 'android'; };
	if (navigator.appVersion.indexOf("Win")!=-1) os = "win";
	if (navigator.appVersion.indexOf("Macintosh")!=-1) os = "osx";

	///////////////////////////////
	// MOBILE BUTTON CLICK
	///////////////////////////////

	$('#menu-btn').on('click', function() {
		$('body').toggleClass('menu-open');
	});

	$('.close-menu-btn').on('click', function() {
		$('body').removeClass('menu-open');
	});

    ///////////////////////////////////////////////
    // TO THE TOP 
    ///////////////////////////////////////////////

    var tttOffset   = 300,
        tttOpacity  = 1200,
        tttDuration = 700,
        $ttt        = $('.ttt');

	$(window).scroll(function(){
		( $(this).scrollTop() > tttOffset ) ? $ttt.addClass('fadeInUp') : $ttt.removeClass('fadeInUp semi-opaque');
		if( $(this).scrollTop() > tttOpacity ) { 
			$ttt.addClass('semi-opaque');
		}
	});

	$ttt.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
			}, tttDuration	
		);
	});

	///////////////////////////////
	// CALL PAGE-SPECIFIC SCRIPTS
	///////////////////////////////

	pageTemplate = $('[data-template]').attr('data-template');

	// Check to see if template is present
	if ( pageTemplate.length > 0 && pageScripts[pageTemplate] ) {

		// Call page-specific scripts
			// Pass in smoothState as arg so that page-specific scripts have access
		pageScripts[pageTemplate](os);
	};
});