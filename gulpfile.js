var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    nodemon = require('gulp-nodemon'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),

    input  = {
      'sass': 'source/scss/app.scss',
      'js': 'source/js/**/*.js'
    },

    output = {
      'css': 'public/css',
      'js': 'public/js'
    };

gulp.task('sass', function () {
    gulp.src( input.sass )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        // .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest( output.css ));
});

gulp.task('build-js', function() {
    return gulp.src([
        // 'source/js/jquery-3.2.1.min.js',
        'source/js/slick.min.js',
        'source/js/smoothstate.js',
        'source/js/snap.svg-min.js',
        'source/js/utilities.js',
        'source/js/pages.js',
        'source/js/init.js'])
    //.pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    //only uglify if gulp is ran with '--type production'
    //.pipe(uglify())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest( output.js ));
});

gulp.task('default', ['build-js', 'sass'], function () {
    gulp.watch('source/scss/**/*', { interval: 800 }, ['sass']);
    gulp.watch('source/js/*', { interval: 500 }, ['build-js']);
});